<?php

namespace GetNoticed\CloudFlare\Service;

use Magento\Framework;
use GetNoticed\CloudFlare as CF;
use Cloudflare\API as CFAPI;

class CfApiService implements CF\Api\CfApiServiceInterface
{
    /**
     * @var CF\Helper\Config\GeneralInterface
     */
    private $generalConfig;

    /**
     * @var CFAPI\Auth\APIKeyFactory
     */
    private $apiKeyFactory;

    /**
     * @var CFAPI\Adapter\GuzzleFactory
     */
    private $adapterFactory;

    /**
     * @var CF\Service\CfApiEndpointFactory
     */
    private $endpointFactory;

    public function __construct(
        CF\Helper\Config\GeneralInterface $generalConfig,
        CFAPI\Auth\APIKeyFactory $apiKeyFactory,
        CFAPI\Adapter\GuzzleFactory $adapterFactory,
        CF\Service\CfApiEndpointFactory $endpointFactory
    ) {
        $this->generalConfig = $generalConfig;
        $this->apiKeyFactory = $apiKeyFactory;
        $this->adapterFactory = $adapterFactory;
        $this->endpointFactory = $endpointFactory;
    }

    /**
     * @inheritdoc
     */
    public function getApiKey(): CFAPI\Auth\APIKey
    {
        try {
            return $this->apiKeyFactory->create(
                [
                    'email'  => $this->generalConfig->getEmail(),
                    'apiKey' => $this->generalConfig->getApiKey()
                ]
            );
        } catch (\Error | \Exception $e) {
            throw CF\Exception\CloudFlareApiException::initializationError($e);
        }
    }

    /**
     * @inheritdoc
     */
    public function getAdapter(): CFAPI\Adapter\Adapter
    {
        try {
            return $this->adapterFactory->create(
                [
                    'auth' => $this->getApiKey()
                ]
            );
        } catch (CF\Exception\CloudFlareApiException $e) {
            throw  $e;
        } catch (\Error | \Exception $e) {
            throw CF\Exception\CloudFlareApiException::initializationError($e);
        }
    }

    /**
     * @inheritdoc
     * @return CFAPI\Endpoints\Zones|CFAPI\Endpoints\API
     * @throws Framework\Exception\InputException
     */
    public function getZonesEndpoint(): CFAPI\Endpoints\Zones
    {
        return $this->endpointFactory->create(CFAPI\Endpoints\Zones::class, ['adapter' => $this->getAdapter()]);
    }
}