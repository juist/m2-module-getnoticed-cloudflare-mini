<?php

namespace GetNoticed\CloudFlare\Service;

use GetNoticed\CloudFlare as CF;
use GuzzleHttp as Gz;

class CfZoneService implements CF\Api\CfZoneServiceInterface
{
    /**
     * @var CF\Api\CfApiServiceInterface
     */
    private $cfApiService;

    /**
     * @var CF\Api\CfConverterServiceInterface
     */
    private $converterService;

    public function __construct(
        CF\Api\CfApiServiceInterface $cfApiService,
        CF\Api\CfConverterServiceInterface $converterService
    ) {
        $this->cfApiService = $cfApiService;
        $this->converterService = $converterService;
    }

    /**
     * @inheritdoc
     */
    public function listZones(): array
    {
        try {
            return array_map(
                [$this->converterService, 'convertZone'],
                $this->cfApiService->getZonesEndpoint()->listZones()->result
            );
        } catch (Gz\Exception\ClientException $e) {
            throw CF\Exception\CloudFlareApiException::guzzleException($e);
        } catch (\Error | \Exception $e) {
            throw CF\Exception\CloudFlareApiException::misc($e);
        }
    }

    /**
     * @inheritdoc
     */
    public function getZoneById(string $zoneId): CF\Api\Data\CfZoneInterface
    {
        try {
            return $this->converterService->convertZone(
                $this->cfApiService->getZonesEndpoint()->getZoneById($zoneId)->result
            );
        } catch (Gz\Exception\ClientException $e) {
            throw CF\Exception\CloudFlareApiException::guzzleException($e);
        } catch (\Error | \Exception $e) {
            throw CF\Exception\CloudFlareApiException::misc($e);
        }
    }

    /**
     * @inheritdoc
     */
    public function purgeCache(CF\Api\Data\CfZoneInterface $zone): bool
    {
        return $this->cfApiService->getZonesEndpoint()->cachePurgeEverything($zone->getId());
    }
}