<?php

namespace GetNoticed\CloudFlare\Service;

use Magento\Framework;
use GetNoticed\CloudFlare as CF;

class CfConverterService implements CF\Api\CfConverterServiceInterface
{
    /**
     * @var array
     */
    private $factories = [];

    public function __construct(
        CF\Api\Data\CfZoneInterfaceFactory $cfZoneFactory,
        CF\Api\Data\CfZoneOwnerInterfaceFactory $cfZoneOwnerFactory,
        CF\Api\Data\CfZoneAccountInterfaceFactory $cfZoneAccountFactory,
        CF\Api\Data\CfZonePlanInterfaceFactory $cfZonePlanFactory
    ) {
        $this->factories[CF\Api\Data\CfZoneInterface::class] = $cfZoneFactory;
        $this->factories[CF\Api\Data\CfZoneOwnerInterface::class] = $cfZoneOwnerFactory;
        $this->factories[CF\Api\Data\CfZoneAccountInterface::class] = $cfZoneAccountFactory;
        $this->factories[CF\Api\Data\CfZonePlanInterface::class] = $cfZonePlanFactory;
    }

    /**
     * @inheritdoc
     */
    public function convertZone(\stdClass $response): CF\Api\Data\CfZoneInterface
    {
        /** @var CF\Api\Data\CfZoneInterface|CF\Model\Data\CfZone $zone */
        $zone = $this->factory(CF\Api\Data\CfZoneInterface::class)->create();

        $this->processObject(
            $response,
            function ($key, $value) use ($zone) {
                switch ($key) {
                    case 'id':
                        $zone->setId($value);
                        break;

                    case 'name':
                        $zone->setName($value);
                        break;

                    case 'status':
                        $zone->setStatus($value);
                        break;

                    case 'paused':
                        $zone->setPaused((bool)$value);
                        break;

                    case 'permissions':
                        $zone->setPermissions($value);
                        break;

                    case 'owner':
                        $zone->setOwner($this->convertZoneOwner($value));
                        break;

                    case 'plan':
                        $zone->setPlan($this->convertZonePlan($value));
                        break;

                    case 'account':
                        $zone->setAccount($this->convertZoneAccount($value));
                        break;
                }
            }
        );

        return $zone;
    }

    public function convertZoneOwner(\stdClass $response): CF\Api\Data\CfZoneOwnerInterface
    {
        /** @var CF\Api\Data\CfZoneOwnerInterface|CF\Model\Data\CfZoneOwner $zoneOwner */
        $zoneOwner = $this->factory(CF\Api\Data\CfZoneOwnerInterface::class)->create();

        $this->processObject(
            $response,
            function ($key, $value) use ($zoneOwner) {
                switch ($key) {
                    case 'id':
                        $zoneOwner->setId($value);
                        break;

                    case 'email':
                        $zoneOwner->setEmail($value);
                        break;

                    case 'type':
                        $zoneOwner->setType($value);
                        break;
                }
            }
        );

        return $zoneOwner;
    }

    public function convertZoneAccount(\stdClass $response): CF\Api\Data\CfZoneAccountInterface
    {
        /** @var CF\Api\Data\CfZoneAccountInterface|CF\Model\Data\CfZoneAccount $zoneAccount */
        $zoneAccount = $this->factory(CF\Api\Data\CfZoneAccountInterface::class)->create();

        $this->processObject(
            $response,
            function ($key, $value) use ($zoneAccount) {
                switch ($key) {
                    case 'id':
                        $zoneAccount->setId($value);
                        break;

                    case 'name':
                        $zoneAccount->setName($value);
                        break;
                }
            }
        );

        return $zoneAccount;
    }

    public function convertZonePlan(\stdClass $response): CF\Api\Data\CfZonePlanInterface
    {
        /** @var CF\Api\Data\CfZonePlanInterface|CF\Model\Data\CfZonePlan $zonePlan */
        $zonePlan = $this->factory(CF\Api\Data\CfZonePlanInterface::class)->create();

        $this->processObject(
            $response,
            function ($key, $value) use ($zonePlan) {
                switch ($key) {
                    case 'id':
                        $zonePlan->setId($value);
                        break;

                    case 'name':
                        $zonePlan->setName($value);
                        break;

                    case 'price':
                        $zonePlan->setPrice($value);
                        break;

                    case 'currency':
                        $zonePlan->setCurrency($value);
                        break;
                }
            }
        );

        return $zonePlan;
    }

    /**
     * @param string $interfaceName
     *
     * @return object
     */
    private function factory(string $interfaceName)
    {
        return $this->factories[$interfaceName];
    }

    /**
     * @param \stdClass $object
     * @param callable  $callback
     *
     * @throws Framework\Exception\InputException
     */
    private function processObject(\stdClass $object, callable $callback): void
    {
        if (is_object($object) !== true) {
            throw new Framework\Exception\InputException(
                __('%1::%2 only accepts objects as first argument', __CLASS__, __METHOD__)
            );
        }

        if (is_callable($callback) !== true) {
            throw new Framework\Exception\InputException(
                __('%1::%2 has not been given a valid callback', __CLASS__, __METHOD__)
            );
        }

        foreach (get_object_vars($object) as $key => $value) {
            $callback($key, $value);
        }
    }
}