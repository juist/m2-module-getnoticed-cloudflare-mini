<?php

namespace GetNoticed\CloudFlare\Service;

use Magento\Framework;
use CloudFlare\API as CFAPI;

class CfApiEndpointFactory implements Framework\ObjectManager\FactoryInterface
{
    const ZONES = CFAPI\Endpoints\Zones::class;

    /**
     * @var Framework\ObjectManagerInterface
     */
    private $objectManager;

    public function __construct(Framework\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param string $requestedType
     * @param array  $arguments
     *
     * @return CFAPI\Endpoints\API
     * @throws Framework\Exception\InputException
     */
    public function create($requestedType, array $arguments = [])
    {
        if (\is_subclass_of($requestedType, CFAPI\Endpoints\API::class) !== true) {
            throw new Framework\Exception\InputException(
                __('This factory can only create objects of type "%1"', CFAPI\Endpoints\API::class)
            );
        }

        return $this->objectManager->create($requestedType, $arguments);
    }

    public function setObjectManager(Framework\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
}