<?php

namespace GetNoticed\CloudFlare\Model\Data;

use GetNoticed\CloudFlare as CF;

class CfZone implements CF\Api\Data\CfZoneInterface
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var CF\Api\Data\CfZoneOwnerInterface
     */
    private $owner;

    /**
     * @var CF\Api\Data\CfZoneAccountInterface
     */
    private $account;

    /**
     * @var string[]
     */
    private $permissions = [];

    /**
     * @var CF\Api\Data\CfZonePlanInterface
     */
    private $plan;

    /**
     * @var string
     */
    private $status;

    /**
     * @var bool
     */
    private $paused = false;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return CF\Api\Data\CfZoneOwnerInterface
     */
    public function getOwner(): CF\Api\Data\CfZoneOwnerInterface
    {
        return $this->owner;
    }

    /**
     * @param CF\Api\Data\CfZoneOwnerInterface $owner
     *
     * @return $this
     */
    public function setOwner(CF\Api\Data\CfZoneOwnerInterface $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return CF\Api\Data\CfZoneAccountInterface
     */
    public function getAccount(): CF\Api\Data\CfZoneAccountInterface
    {
        return $this->account;
    }

    /**
     * @param CF\Api\Data\CfZoneAccountInterface $account
     *
     * @return $this
     */
    public function setAccount(CF\Api\Data\CfZoneAccountInterface $account): self
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @return string[]
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @param string[] $permissions
     *
     * @return $this
     */
    public function setPermissions(array $permissions): self
    {
        $this->permissions = $permissions;

        return $this;
    }

    /**
     * @return CF\Api\Data\CfZonePlanInterface
     */
    public function getPlan(): CF\Api\Data\CfZonePlanInterface
    {
        return $this->plan;
    }

    /**
     * @param CF\Api\Data\CfZonePlanInterface $plan
     *
     * @return $this
     */
    public function setPlan(CF\Api\Data\CfZonePlanInterface $plan): self
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return $this
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->getStatus() === 'active' && $this->isPaused() !== true;
    }

    /**
     * @return bool
     */
    public function isPaused(): bool
    {
        return $this->paused;
    }

    /**
     * @param bool $paused
     *
     * @return $this
     */
    public function setPaused(bool $paused): self
    {
        $this->paused = $paused;

        return $this;
    }
}