<?php

namespace GetNoticed\CloudFlare\Exception;

use Magento\Framework;

class EmptyConfigException extends Framework\Exception\LocalizedException
{
    public static function missingConfig(string $path, string $label): self
    {
        return new self(
            __(
                'Configuration is incomplete / missing / empty: %1 (path: %2)',
                $label,
                $path
            )
        );
    }

    public static function missingStoreConfig(string $path, string $label, string $type, string $scope): self
    {
        return new self(
            __(
                'Store Configuration is incomplete / missing / empty: %1 (path: %2, store: %3/%4)',
                $label,
                $path,
                $type,
                $scope
            )
        );
    }
}