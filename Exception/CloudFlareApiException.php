<?php

namespace GetNoticed\CloudFlare\Exception;

use Magento\Framework;
use GuzzleHttp as Gz;

class CloudFlareApiException extends Framework\Exception\LocalizedException
{
    /**
     * @param \Error|\Exception $e
     *
     * @return self
     */
    public static function initializationError($e): self
    {
        return new self(
            __(
                'Error during initialization of the API client: %1',
                $e instanceof Framework\Exception\LocalizedException || $e instanceof \Error
                    ? $e->getMessage() : 'Unknown error'
            )
        );
    }

    public static function guzzleException(Gz\Exception\RequestException $e): self
    {
        // Create a default exception to use
        $defaultException = new self(
            __(
                'Unknown API error, HTTP status code: %1',
                $e->getResponse()->getStatusCode()
            )
        );

        // Try to get additional information from response
        try {
            $bodyContents = $e->getResponse()->getBody()->getContents();
            $response = \json_decode($bodyContents, true);

            if (is_array($response)) {
                return new self(
                    __(
                        'API error, message(s): %1',
                        sprintf(
                            '%s* %s', PHP_EOL,
                            implode(
                                sprintf('%s* ', PHP_EOL),
                                array_map([self::class, 'convertCfError'], $response['errors'])
                            )
                        )
                    )
                );
            }
        } catch (\Error | \Exception $e) {
            return $defaultException;
        }

        return $defaultException;
    }

    /**
     * @param \Error|\Exception $e
     *
     * @return $this
     */
    public static function misc($e): self
    {
        return new self(__('Unknown error occurred: %1', $e->getMessage()));
    }

    /**
     * @param array $errorData
     *
     * @return Framework\Phrase
     */
    public static function convertCfError(array $errorData): Framework\Phrase
    {
        return __('%1: %2', $errorData['code'], __($errorData['message']));
    }
}