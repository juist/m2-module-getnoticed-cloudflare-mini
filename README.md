# Get.Noticed CloudFlare

# General readme

This module allows the administrative parties of a webshop to manage their CloudFlare status.

It can be enabled / disabled for each store (website scope) in an installation.
If configured, it will be possible to purge the cache through the admin panel.

# Contributors
## CloudFlare documentation

* **API documentation:** [api.cloudflare.com](https://api.cloudflare.com/)
* **CloudFlare PHP SDK:** [github.com/cloudflare/cloudflare-php](https://github.com/cloudflare/cloudflare-php)