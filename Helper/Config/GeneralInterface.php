<?php

namespace GetNoticed\CloudFlare\Helper\Config;

use GetNoticed\CloudFlare as CF;

interface GeneralInterface
{
    /**
     * @return bool
     */
    public function isModuleEnabled(): bool;

    /**
     * @return string
     * @throws CF\Exception\EmptyConfigException
     */
    public function getEmail(): string;

    /**
     * @return string
     * @throws CF\Exception\EmptyConfigException
     */
    public function getApiKey(): string;
}