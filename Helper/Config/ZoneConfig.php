<?php

namespace GetNoticed\CloudFlare\Helper\Config;

use Magento\Framework;
use Magento\Store;

class ZoneConfig extends Framework\App\Helper\AbstractHelper implements ZoneConfigInterface
{
    const XML_PATH_BASE = 'getnoticed_cloudflare/zone';
    const XML_PATH_ZONE_ENABLED = '%s/zone_enabled';
    const XML_PATH_ZONE_ID = '%s/zone_id';

    public function isZoneEnabled(string $type = null, ?string $code = null): bool
    {
        return $this->scopeConfig->isSetFlag(
            sprintf(self::XML_PATH_ZONE_ENABLED, self::XML_PATH_BASE),
            $type ?: Store\Model\ScopeInterface::SCOPE_STORE,
            $code
        );
    }

    public function getZoneId(string $type = null, ?string $code = null): string
    {
        return $this->scopeConfig->getValue(
            sprintf(self::XML_PATH_ZONE_ID, self::XML_PATH_BASE),
            $type ?: Store\Model\ScopeInterface::SCOPE_STORE,
            $code
        );
    }
}