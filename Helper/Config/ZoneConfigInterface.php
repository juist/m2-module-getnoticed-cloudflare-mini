<?php

namespace GetNoticed\CloudFlare\Helper\Config;

interface ZoneConfigInterface
{
    public function isZoneEnabled(?string $type = null, ?string $code = null): bool;

    public function getZoneId(?string $type = null, ?string $code = null): string;
}