<?php

namespace GetNoticed\CloudFlare\Helper\Config;

use GetNoticed\CloudFlare as CF;
use Magento\Framework;
use Magento\Store;

class General extends Framework\App\Helper\AbstractHelper implements GeneralInterface
{
    const XML_PATH_BASE = 'getnoticed_cloudflare/general';
    const XML_PATH_ENABLED = '%s/enabled';
    const XML_PATH_EMAIL = '%s/email';
    const XML_PATH_API_KEY = '%s/api_key';

    /**
     * @inheritdoc
     */
    public function isModuleEnabled(): bool
    {
        return $this->scopeConfig->isSetFlag(sprintf(self::XML_PATH_ENABLED, self::XML_PATH_BASE));
    }

    /**
     * @inheritdoc
     */
    public function getEmail(): string
    {
        $path = sprintf(self::XML_PATH_EMAIL, self::XML_PATH_BASE);
        $label = __('E-mail address');

        $email = $this->scopeConfig->getValue($path);

        if (empty($email)) {
            throw CF\Exception\EmptyConfigException::missingConfig($path, $label);
        }

        return $email;
    }

    /**
     * @inheritdoc
     */
    public function getApiKey(): string
    {
        $path = sprintf(self::XML_PATH_API_KEY, self::XML_PATH_BASE);
        $label = __('API key');

        $apiKey = $this->scopeConfig->getValue($path);

        if (empty($apiKey)) {
            throw CF\Exception\EmptyConfigException::missingConfig($path, $label);
        }

        return $apiKey;
    }
}