<?php

namespace GetNoticed\CloudFlare\Source;

use Magento\Framework;
use GetNoticed\CloudFlare as CF;

class CfZoneList implements Framework\Option\ArrayInterface
{
    /**
     * @var CF\Api\CfZoneServiceInterface
     */
    private $cfZoneService;

    public function __construct(
        CF\Api\CfZoneServiceInterface $cfZoneService
    ) {
        $this->cfZoneService = $cfZoneService;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        try {
            return array_map(
                function (CF\Api\Data\CfZoneInterface $zone) {
                    return [
                        'value' => $zone->isActive() ? $zone->getId() : null,
                        'label' => __(
                            '%1%2 (owner: %3, account: %4)',
                            $zone->getName(),
                            $zone->isActive() ? null : sprintf(' [%s]', __('INACTIVE')),
                            $zone->getOwner()->getEmail(),
                            $zone->getAccount()->getName()
                        )
                    ];
                },
                $this->cfZoneService->listZones()
            );
        } catch (CF\Exception\CloudFlareApiException $e) {
            return [['value' => '', 'label' => __('Please configure API first (%1)', $e->getMessage())]];
        }
    }
}