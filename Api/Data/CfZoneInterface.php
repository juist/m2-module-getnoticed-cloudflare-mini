<?php

namespace GetNoticed\CloudFlare\Api\Data;

use GetNoticed\CloudFlare as CF;

interface CfZoneInterface
{
    public function getId(): string;

    public function getName(): string;

    public function getOwner(): CF\Api\Data\CfZoneOwnerInterface;

    public function getAccount(): CF\Api\Data\CfZoneAccountInterface;

    /**
     * @return string[]
     */
    public function getPermissions(): array;

    public function getPlan(): CF\Api\Data\CfZonePlanInterface;

    public function getStatus(): string;

    /**
     * this.getStatus === 'active' && !this.isPaused()
     *
     * @return bool
     */
    public function isActive(): bool;

    public function isPaused(): bool;
}