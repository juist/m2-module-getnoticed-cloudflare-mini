<?php

namespace GetNoticed\CloudFlare\Api\Data;

interface CfZoneOwnerInterface
{
    public function getId(): string;

    public function getEmail(): string;

    public function getType(): string;
}