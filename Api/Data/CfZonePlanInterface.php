<?php

namespace GetNoticed\CloudFlare\Api\Data;

interface CfZonePlanInterface
{
    public function getId(): string;

    public function getName(): string;

    public function getPrice(): float;

    public function getCurrency(): string;
}