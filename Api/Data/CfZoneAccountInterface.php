<?php

namespace GetNoticed\CloudFlare\Api\Data;

interface CfZoneAccountInterface
{
    public function getId(): string;

    public function getName(): string;
}