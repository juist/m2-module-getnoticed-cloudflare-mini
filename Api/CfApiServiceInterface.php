<?php

namespace GetNoticed\CloudFlare\Api;

use GetNoticed\CloudFlare as CF;
use Cloudflare\API as CFAPI;

interface CfApiServiceInterface
{
    const CONSOLE_CMD_PREFIX = 'gn:cf-api';

    /**
     * @return CFAPI\Auth\APIKey
     * @throws CF\Exception\CloudFlareApiException
     */
    public function getApiKey(): CFAPI\Auth\APIKey;

    /**
     * @return CFAPI\Adapter\Adapter
     * @throws CF\Exception\CloudFlareApiException
     */
    public function getAdapter(): CFAPI\Adapter\Adapter;

    /**
     * @return CFAPI\Endpoints\Zones|CFAPI\Endpoints\API
     * @throws CF\Exception\CloudFlareApiException
     */
    public function getZonesEndpoint(): CFAPI\Endpoints\Zones;
}