<?php

namespace GetNoticed\CloudFlare\Api;

use Magento\Framework;
use GetNoticed\CloudFlare as CF;

interface CfConverterServiceInterface
{
    /**
     * @param \stdClass $response
     *
     * @return CF\Api\Data\CfZoneInterface
     * @throws Framework\Exception\LocalizedException
     * @throws CF\Exception\CloudFlareApiException
     */
    public function convertZone(\stdClass $response): CF\Api\Data\CfZoneInterface;

    /**
     * @param \stdClass $response
     *
     * @return CF\Api\Data\CfZoneOwnerInterface
     * @throws Framework\Exception\LocalizedException
     * @throws CF\Exception\CloudFlareApiException
     */
    public function convertZoneOwner(\stdClass $response): CF\Api\Data\CfZoneOwnerInterface;

    /**
     * @param \stdClass $response
     *
     * @return CF\Api\Data\CfZoneAccountInterface
     * @throws Framework\Exception\LocalizedException
     * @throws CF\Exception\CloudFlareApiException
     */
    public function convertZoneAccount(\stdClass $response): CF\Api\Data\CfZoneAccountInterface;

    /**
     * @param \stdClass $response
     *
     * @return CF\Api\Data\CfZonePlanInterface
     * @throws Framework\Exception\LocalizedException
     * @throws CF\Exception\CloudFlareApiException
     */
    public function convertZonePlan(\stdClass $response): CF\Api\Data\CfZonePlanInterface;
}