<?php

namespace GetNoticed\CloudFlare\Api;

use GetNoticed\CloudFlare as CF;

interface CfZoneServiceInterface
{
    /**
     * @return CF\Api\Data\CfZoneInterface[]
     * @throws CF\Exception\CloudFlareApiException
     */
    public function listZones(): array;

    /**
     * @param string $zoneId
     *
     * @return CF\Api\Data\CfZoneInterface
     * @throws CF\Exception\CloudFlareApiException
     */
    public function getZoneById(string $zoneId): CF\Api\Data\CfZoneInterface;

    /**
     * @param CF\Api\Data\CfZoneInterface $zone
     *
     * @return bool
     * @throws CF\Exception\CloudFlareApiException
     */
    public function purgeCache(CF\Api\Data\CfZoneInterface $zone): bool;
}