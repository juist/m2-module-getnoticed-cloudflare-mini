<?php

namespace GetNoticed\CloudFlare\Console\Command\Api;

use Magento\Store;
use GetNoticed\CloudFlare as CF;

class ApiContext
{
    /**
     * @var CF\Helper\Config\GeneralInterface
     */
    private $generalConfig;

    /**
     * @var CF\Api\CfZoneServiceInterface
     */
    private $cfZoneService;

    /**
     * @var Store\Model\StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        CF\Helper\Config\GeneralInterface $generalConfig,
        CF\Api\CfZoneServiceInterface $cfZoneService,
        Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->generalConfig = $generalConfig;
        $this->cfZoneService = $cfZoneService;
        $this->storeManager = $storeManager;
    }

    /**
     * @return CF\Helper\Config\GeneralInterface
     */
    public function getGeneralConfig(): CF\Helper\Config\GeneralInterface
    {
        return $this->generalConfig;
    }

    /**
     * @return CF\Api\CfZoneServiceInterface
     */
    public function getCfZoneService(): CF\Api\CfZoneServiceInterface
    {
        return $this->cfZoneService;
    }

    /**
     * @return Store\Model\StoreManagerInterface
     */
    public function getStoreManager(): Store\Model\StoreManagerInterface
    {
        return $this->storeManager;
    }
}