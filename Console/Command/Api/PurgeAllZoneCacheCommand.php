<?php

namespace GetNoticed\CloudFlare\Console\Command\Api;

use Magento\Framework;
use Magento\Store;
use GetNoticed\CloudFlare as CF;
use Symfony\Component\Console;

class PurgeAllZoneCacheCommand extends AbstractApiCommand
{
    const CMD_NAME = CF\Api\CfApiServiceInterface::CONSOLE_CMD_PREFIX . ':zones:purge-all';

    /**
     * @var CF\Helper\Config\ZoneConfigInterface
     */
    private $zoneConfig;

    public function __construct(
        CF\Console\Command\Api\ApiContext $context,
        CF\Helper\Config\ZoneConfigInterface $zoneConfig
    ) {
        parent::__construct($context);

        $this->zoneConfig = $zoneConfig;
    }

    protected function configure()
    {
        $this->setName(self::CMD_NAME);
    }

    /**
     * @param Console\Input\InputInterface $input
     * @param Console\Output\OutputInterface $output
     *
     * @return int|void|null
     * @throws CF\Exception\CloudFlareApiException
     * @throws Framework\Exception\LocalizedException
     */
    public function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        $io = new Console\Style\SymfonyStyle($input, $output);
        $purgedZoneIds = [];
        $success = $failed = $skipped = 0;

        foreach ($this->storeManager->getStores() as $store) {
            $isEnabled = $this->zoneConfig->isZoneEnabled(Store\Model\ScopeInterface::SCOPE_STORE, $store->getCode());

            if ($isEnabled !== true) {
                $skipped++;
                continue;
            }

            $zoneId = $this->zoneConfig->getZoneId(Store\Model\ScopeInterface::SCOPE_STORE, $store->getCode());
            if (in_array($zoneId, $purgedZoneIds)) {
                $skipped++;
                continue;
            }

            $zone = $this->cfZoneService->getZoneById($zoneId);

            if ($this->cfZoneService->purgeCache($zone)) {
                $success++;
            } else {
                $failed++;
            }
        }

        $io->success(
            __('%1 successfully processed and %2 failed, %3 store views skipped', $success, $failed, $skipped)
        );
    }
}