<?php

namespace GetNoticed\CloudFlare\Console\Command\Api;

use GetNoticed\CloudFlare as CF;
use Symfony\Component\Console;

class ListZonesCommand extends AbstractApiCommand
{
    const CMD_NAME = CF\Api\CfApiServiceInterface::CONSOLE_CMD_PREFIX . ':zones:list';

    protected function configure()
    {
        $this->setName(self::CMD_NAME);
    }

    /**
     * @param Console\Input\InputInterface   $input
     * @param Console\Output\OutputInterface $output
     *
     * @return void
     * @throws CF\Exception\CloudFlareApiException
     */
    protected function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        $io = new Console\Style\SymfonyStyle($input, $output);
        $zones = $this->cfZoneService->listZones();

        $io->table(
            [
                __('ID'),
                __('Name'),
                __('Status'),
                __('Paused'),
                __('Active'),
                __('Account'),
                __('Owner'),
                __('Owner type'),
                __('Plan'),
                __('Plan costs')
            ],
            array_map(
                function (CF\Api\Data\CfZoneInterface $zone) {
                    return [
                        $zone->getId(),
                        $zone->getName(),
                        $zone->getStatus(),
                        $zone->isPaused() ? __('Yes') : __('No'),
                        $zone->isActive() ? __('Yes') : __('No'),
                        $zone->getAccount()->getName(),
                        $zone->getOwner()->getEmail(),
                        $zone->getOwner()->getType(),
                        $zone->getPlan()->getName(),
                        $zone->getPlan()->getPrice() . ' ' . $zone->getPlan()->getCurrency()
                    ];
                },
                $zones
            )
        );
    }
}