<?php

namespace GetNoticed\CloudFlare\Console\Command\Api;

use Magento\Framework;
use Magento\Store;
use GetNoticed\CloudFlare as CF;
use Symfony\Component\Console;

class PurgeZoneCacheCommand extends AbstractApiCommand
{
    const CMD_NAME = CF\Api\CfApiServiceInterface::CONSOLE_CMD_PREFIX . ':zones:purge';
    const ARG_STORE_ID = 'store-id';

    /**
     * @var CF\Helper\Config\ZoneConfigInterface
     */
    private $zoneConfig;

    public function __construct(
        CF\Console\Command\Api\ApiContext $context,
        CF\Helper\Config\ZoneConfigInterface $zoneConfig
    ) {
        parent::__construct($context);

        $this->zoneConfig = $zoneConfig;
    }

    protected function configure()
    {
        $this
            ->setName(self::CMD_NAME)
            ->setDefinition(
                new Console\Input\InputDefinition(
                    [
                        new Console\Input\InputArgument(
                            self::ARG_STORE_ID,
                            Console\Input\InputArgument::OPTIONAL,
                            'Enter a store for which the CloudFlare cache must be purged (zone will be determined from config)'
                        )
                    ]
                )
            );
    }

    /**
     * @param Console\Input\InputInterface   $input
     * @param Console\Output\OutputInterface $output
     *
     * @return int|void|null
     * @throws CF\Exception\CloudFlareApiException
     * @throws Framework\Exception\LocalizedException
     */
    public function execute(Console\Input\InputInterface $input, Console\Output\OutputInterface $output)
    {
        $io = new Console\Style\SymfonyStyle($input, $output);
        $store = $this->requestStore($input->getArgument(self::ARG_STORE_ID), $io);
        $isEnabled = $this->zoneConfig->isZoneEnabled(Store\Model\ScopeInterface::SCOPE_STORE, $store->getCode());

        if ($isEnabled !== true) {
            throw new Framework\Exception\LocalizedException(
                __('There is no active zone configured for this store.')
            );
        }

        $zoneId = $this->zoneConfig->getZoneId(Store\Model\ScopeInterface::SCOPE_STORE, $store->getCode());
        $zone = $this->cfZoneService->getZoneById($zoneId);

        if ($this->cfZoneService->purgeCache($zone)) {
            $io->success(
                __(
                    'Cache for zone %1 (store: %2, code: %3, id: %4) purged.',
                    $zone->getName(),
                    $store->getName(),
                    $store->getCode(),
                    $store->getId()
                )
            );
        } else {
            $io->error(__('Unknown failure during cache purge!'));
        }
    }
}