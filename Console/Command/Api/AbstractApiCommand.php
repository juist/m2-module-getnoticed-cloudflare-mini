<?php

namespace GetNoticed\CloudFlare\Console\Command\Api;

use Magento\Framework;
use Magento\Store;
use GetNoticed\CloudFlare as CF;
use Symfony\Component\Console;

abstract class AbstractApiCommand extends Console\Command\Command
{
    /**
     * @var CF\Helper\Config\GeneralInterface
     */
    protected $generalConfig;

    /**
     * @var CF\Api\CfZoneServiceInterface
     */
    protected $cfZoneService;

    /**
     * @var Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    public function __construct(
        CF\Console\Command\Api\ApiContext $context
    ) {
        parent::__construct();

        $this->generalConfig = $context->getGeneralConfig();
        $this->cfZoneService = $context->getCfZoneService();
        $this->storeManager = $context->getStoreManager();
    }

    /**
     * @param string|null                $identifier
     * @param Console\Style\SymfonyStyle $io
     *
     * @return Store\Api\Data\StoreInterface|null
     */
    protected function requestStore(?string $identifier, Console\Style\SymfonyStyle $io)
    {
        do {
            try {
                if (empty($identifier)) {
                    throw new Framework\Exception\LocalizedException(
                        __('Please enter a valid identifier')
                    );
                }

                $store = $this->storeManager->getStore($identifier);
            } catch (Framework\Exception\LocalizedException $e) {
                $store = null;
                $io->warning($e->getMessage());
            }

            if (!$store instanceof Store\Api\Data\StoreInterface || $store->getId() === null) {
                $identifier = $io->ask('Please enter/select a valid store');
            }
        } while (!$store instanceof Store\Api\Data\StoreInterface || $store->getId() === null);

        return $store;
    }
}